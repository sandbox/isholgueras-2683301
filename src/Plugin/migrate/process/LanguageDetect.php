<?php
/**
 * @file
 * Contains \Drupal\language_detect\Plugin\migrate\process\LanguageDetect.
 */
namespace Drupal\language_detect\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use TextLanguageDetect\TextLanguageDetect;

/**
 * Process a text and return it's language.
 *
 * @MigrateProcessPlugin(
 *   id = "language_detect"
 * )
 */
class LanguageDetect extends ProcessPluginBase {

  const LanguagesLimit = 1;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (empty($value)) {
      return NULL;
    }
    $string = implode(". ", $value);

    $l = new TextLanguageDetect();
    //return 2-letter language codes only
    $l->setNameMode(2);
    $result = $l->detect($string, self::LanguagesLimit);

    $langcode = $this->convertToDrupalLangcode(array_keys($result)[0]);

    if (!empty($langcode)) {

      return $langcode;
    }
    else {
      return NULL;
    }

  }

  private function convertToDrupalLangcode($unicode) {
    $lm = \Drupal::languageManager();
    $languages = $lm->getLanguages();
    $id_language = "";
    foreach ($languages as $langcode => $language) {
      if (empty($id_language)) {
        $id_language = $langcode;
      }
      unset($languages[$langcode]);
      $short_langcode = substr($langcode, 0, 2);
      $languages[$short_langcode] = $language;
    }

    if (isset($languages[$unicode])) {
      $id_language = $languages[$unicode]->getId();
    }

    return $id_language;

  }
}